
name := "spark helloworld"

version := "1.0.0"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
	"org.apache.spark" %% "spark-core" % "1.2.0" % "provided",
	"org.scalatest" %% "scalatest" % "1.9.2" % "test->default"
)

fork := true

testOptions in Test += Tests.Argument("-oF")

