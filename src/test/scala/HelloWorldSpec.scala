import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.WordSpec
import org.scalatest.matchers.ShouldMatchers


class HelloWorldSpec extends WordSpec with ShouldMatchers {
  val testData = Seq("Hello World", "Hello", "World Hello")

  def withSC[R](master: String)(f: SparkContext => R): R = {
    val sc = new SparkContext(
      new SparkConf().setAppName("HelloWorld test").setMaster(master)
    )

    try {
      f(sc)
    } finally {
      sc.stop()
    }
  }

  "HelloWorld" should {
    "run locally using single thread" in {
      withSC("local[1]") { sc =>
        val lines = sc.makeRDD(testData)

        HelloWorld.getTop10WordCount(lines) should equal(Seq(("Hello", 3), ("World", 2)))
      }
    }

    "run locally using as many thread as there is CPU cores" in {
      withSC("local[*]") { sc =>
        val lines = sc.makeRDD(testData)

        HelloWorld.getTop10WordCount(lines) should equal(Seq(("Hello", 3), ("World", 2)))
      }
    }
  }
}
