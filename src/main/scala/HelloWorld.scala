import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD


object HelloWorld {
  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("Spark HelloWorld Application")

    val sc = new SparkContext(conf)

    val lines = sc.makeRDD(Seq("Hello World", "Hello", "World", "World Hello"))

    println("Top 10 word count: %s".format(getTop10WordCount(lines)))
  }

  /**
   * Gets 10 most frequent words.
   *
   * @param lines
   * @return
   */
  def getTop10WordCount(lines: RDD[String]): Seq[(String, Long)] = {
    val wordCount = lines.flatMap(
      _.split(' ')
    ).filter(
        _.nonEmpty
      ).map(
        (_, 1L)
      ).reduceByKey(
        _ + _
      ).sortBy(_._2, false)

    wordCount.take(10)
  }
}
